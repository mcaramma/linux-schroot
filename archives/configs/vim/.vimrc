" Pathogen load
filetype off

call pathogen#infect()
call pathogen#helptags()

" Language plugins
filetype plugin indent on
syntax on
set tabstop=4
set shiftwidth=4
set expandtab

" Autocomplete menu
set wildmode=longest,list,full
set wildmenu

" Colors theme
colors desert

" Tabs
nnoremap <C-t> <Esc>:tabnew<CR>:edit 
nnoremap <C-n> <Esc>:tabnext<CR>
nnoremap <C-p> <Esc>:tabprev<CR>
nnoremap <C-w> <Esc>:tabclose<CR>
nnoremap <C-o> <Esc>:tabfind .<CR>
nnoremap <C-s> <Esc>:sh<CR>

" Move inside next window
nmap <silent> <C-m> :wincmd w<CR>

" Un/fold all
nmap <C-f> zM
nmap <C-u> zR
