# Aerospike database configuration file.

service {
	user root
	group root
	paxos-single-replica-limit 1 # Number of nodes where the replica count is automatically reduced to 1.
	pidfile /var/run/aerospike/asd.pid
	service-threads 4
	transaction-queues 4
	transaction-threads-per-queue 4
	proto-fd-max 15000
	paxos-protocol v4
}

cluster {
    mode dynamic
    self-group-id 201
}

logging {
	# Log file must be an absolute path.
	file /var/log/aerospike/aerospike.log {
		context any info
	}
}

network {
	service {
		address any
		port 3000
	}

	heartbeat {
		mode mesh
		address @@NODE_PRIVATE_IP@@
		port 3002
		@@MESH_ADDRESS@@
		interval 150
		timeout 10
	}

	fabric {
		port 3001
	}

	info {
		port 3003
	}
}

namespace piloto {
        replication-factor 2
        memory-size 4G
        default-ttl 0d # 30 days, use 0 to never expire/evict.

        storage-engine device {
                file /opt/aerospike/data/piloto.dat
                filesize 2G
                data-in-memory false # Store data in memory in addition to file.
        }
}
