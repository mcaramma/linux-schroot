#!/bin/bash -x

# must have at least 4 arguments
[ $# -lt 2 ] && {
    echo "Usage: $0 system vgname [node lvsize [-- params]]

    system		- one of ansible | celery | elasticsearch | orientdb | fanery | glusterfs | icinga | logstash | proxy
    vgname		- volume group name (ej. vg1)
    node		- sees node name (ej. hv-fl-001)
    lvsize		- logical volume size (ej. 10G)
    params		- params list passed to setup-{system} script (ej. setup-elasticsearch {node} @params)

    note:
    - if node is not provided the last configuration step is not executed.
    - compressed chroot environments are stored inside $(dirname $0)/bakcups/ directory.
    "
    exit 1
}

# helper function (fail hard)
die () {
  echo "FAIL: $@"
  exit 1
}

# organize input params
ALL_ARGS="$@"
SYSTEM=$1
shift
VG_NAME=$1
shift
NODE=$1
shift
LV_SIZE=$1
shift
LV_TYPE=xfs

# set release based on selected system
case "$SYSTEM" in
  ansible)
    RELEASE=trusty
    ;;
  aerospike|rethinkdb|lizardfs|xtreemfs)
    RELEASE=wheezy
    ;;
  *)
    RELEASE=jessie
    ;;
esac

# should pack optional parameters?
case "$1" in
  "--")
    shift
    PARAMS="$@"
    ;;
  "")
    ;;
  *)
    die "'--' must prefix arguments <$@>"
    ;;
esac

# work with absolute paths
REAL_PATH="$(readlink -e $0)"
BASE_DIR="$(dirname $REAL_PATH)"

# create backup dir
mkdir -p $BASE_DIR/backups 2> /dev/null

# get most recent xz build
XZ_RELEASE="$(readlink -e $(ls -1 $BASE_DIR/backups/${RELEASE}_*.xz | tail -1) 2> /dev/null)"
XZ_SYSTEM="$(readlink -e $(ls -1 $BASE_DIR/backups/${SYSTEM}_*.xz | tail -1) 2> /dev/null)"

# verify system install and setup script exists
INST_SCRIPT="$(readlink -e $BASE_DIR/scripts/install-${SYSTEM} 2> /dev/null)"
CONF_SCRIPT="$(readlink -e $BASE_DIR/scripts/setup-${SYSTEM} 2> /dev/null)"
[ -z "$INST_SCRIPT" ] && die "install script missing $BASE_DIR/scripts/install-${SYSTEM}"
([ ! -z "$NODE" ] && [ -z "$CONF_SCRIPT" ]) && die "setup script missing $BASE_DIR/scripts/setup-${SYSTEM}"

# debootstrap release and/or build system xz build if none found
# and re-run the script with full params
[ -z "$XZ_SYSTEM" ] && {
  [ -z "$XZ_RELEASE" ] && {
    [ -e /dev/$VG_NAME/$RELEASE ] && lvremove -f /dev/$VG_NAME/$RELEASE
    $BASE_DIR/scripts/schroot-debootstrap $RELEASE $VG_NAME $RELEASE 1G || die "debootstrap $RELEASE"
    $REAL_PATH $ALL_ARGS
    exit
  }
  [ -e /dev/$VG_NAME/$SYSTEM ] && lvremove -f /dev/$VG_NAME/$SYSTEM
  $BASE_DIR/scripts/schroot-build $XZ_RELEASE $SYSTEM $VG_NAME $SYSTEM 3G $INST_SCRIPT || die "build $SYSTEM"
  schroot -b -c source:$SYSTEM -n $SYSTEM -u root --directory /root
  $REAL_PATH $ALL_ARGS
  exit
}

# build node xz file and create schroot environment
[ ! -z "$NODE" ] && {
  XZ_NODE="$BASE_DIR/backups/${NODE}_${SYSTEM}_$(date +%s).xz"

  [ -e /dev/$VG_NAME/$SYTEM ] && lvremove -f /dev/$VG_NAME/$SYSTEM

  $BASE_DIR/scripts/schroot-build $XZ_SYSTEM $SYSTEM $VG_NAME $SYSTEM $LV_SIZE $CONF_SCRIPT $SYSTEM $NODE $PARAMS || die "setup $SYSTEM"

  mv "$(readlink -e $(ls -1 $BASE_DIR/backups/${SYSTEM}_*.xz | tail -1))" $XZ_NODE || die "wrong file"
  ls -lh $XZ_RELEASE $XZ_SYSTEM $XZ_NODE
}
